﻿using BigDataPOC.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Results;

namespace BigDataPOC.WebAPI.Lib
{
    public static class CommonMethods
    {
        public static BadRequestErrorMessageResult LogAndReturnBadRequest(System.Web.Http.ApiController controller,string userId, string source, string errMsg)
        {
            Logger.AddLog(new Log()
            {
                UserId = userId,
                LogType = LOG_Type.Error,
                Description = errMsg,
                Source = source
            });
            return new BadRequestErrorMessageResult(errMsg,controller);
        }

        public static string GetUserByAuthToken(string authToken)
        {
            return "AI_Framework";
        }

        public static bool IsRequestValid(HttpRequestMessage request, out string userId)
        {
            userId = String.Empty;
            IEnumerable<string> authJsonInputString;

            if (!request.Headers.TryGetValues("AuthToken", out authJsonInputString))
                return false;
            else
            {
                userId = GetUserByAuthToken(authJsonInputString.ElementAt(0));

                if (!String.IsNullOrEmpty(userId))
                {
                    return true;
                }
                else
                    return false;

            }

        }

    }
}