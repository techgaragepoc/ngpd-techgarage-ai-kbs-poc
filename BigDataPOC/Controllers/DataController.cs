﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Http.Results;
using Newtonsoft.Json;
using BigDataPOC.HadoopAccess;
using BigDataPOC.Logging;
using BigDataPOC.WebAPI.Entity;
using BigDataPOC.WebAPI.Lib;
using System.Net.Http;

namespace BigDataPOC.WebAPI.Controllers
{
    public class DataController : ApiController
    {


        int maxrec = 1000;
        string CurrentUserId = string.Empty;

        protected internal virtual JsonTextActionResult JsonText(string jsonText)
        {
            return new JsonTextActionResult(Request, jsonText);
        }

        [HttpGet]
        [Route("api/data/GetEntityData")]
        public IHttpActionResult GetEntityData()
        {
            try
            {
                var request = this.Request;

                //if (!CommonMethods.IsRequestValid(request, out this.CurrentUserId))
                //    return BadRequest("authentication failed");

                var doublequotes = "\"";
                var jsonParamString = String.Empty;
                var queryStringDict = Request.GetQueryNameValuePairs();

                if (queryStringDict==null || queryStringDict.Count()==0)
                    return BadRequest("querystring is missing");


                var entityString = String.Empty;
                var paramString = String.Empty;

                foreach (var param in queryStringDict)
                {
                    if (param.Key.ToLower() == "entity")
                    {
                        entityString = (doublequotes + param.Key + doublequotes + ":" + doublequotes + param.Value + doublequotes);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(paramString))
                        {
                            paramString += ",";
                        }

                        paramString += (doublequotes + param.Key + doublequotes + ":" + doublequotes + param.Value + doublequotes);
                    }
                }
                paramString = doublequotes + "parameters" + doublequotes + ": { " + paramString + "}";

                jsonParamString = "{" + entityString +  "," +paramString + "}";
           
                //return BadRequest(jsonParamString);

                var responseData = this.GetSourceData(jsonParamString);

                if (responseData != null && responseData.Success)
                {
                    return JsonText(responseData.Data);
                }
                else
                {
                    var errMessage = (responseData != null) ? responseData.Error : String.Empty;
                    return CommonMethods.LogAndReturnBadRequest(this, this.CurrentUserId, "GetEntityData", errMessage);
                }

            }
            catch (Exception ex)
            {
                return CommonMethods.LogAndReturnBadRequest(this, this.CurrentUserId, "GetEntityData", ex.Message);
            }
        }

    

       [HttpGet]
        public IHttpActionResult GetData()
        {

            try
            {
                var request = this.Request;
      
                if (!CommonMethods.IsRequestValid(request,out this.CurrentUserId))
                    return BadRequest("authentication failed");

                IEnumerable<string> customJsonInputString;
                if (!Request.Headers.TryGetValues("ReqData", out customJsonInputString))
                    return BadRequest("ReqData paramter is missing");

                var jsonParamString = customJsonInputString.ElementAt(0);

                var responseData = this.GetSourceData(jsonParamString);

                if (responseData!=null && responseData.Success)
                {
                    return JsonText(responseData.Data);
                }
                else
                {
                    var errMessage = (responseData != null) ? responseData.Error : String.Empty;
                    return CommonMethods.LogAndReturnBadRequest(this, this.CurrentUserId, "GetData", errMessage);
                }

            }
            catch (Exception ex)
            {
                return CommonMethods.LogAndReturnBadRequest(this, this.CurrentUserId, "GetData", ex.Message);
            }
        }


        private ResponseData GetSourceData(string jsonParamString)
        {
            string result = string.Empty;
            List<dynamic> records = null;

            var jsonObject = new RequestData();
            JsonConvert.PopulateObject(jsonParamString, jsonObject);

            if (!String.IsNullOrEmpty(jsonObject.entity))
            {

                var entityname = jsonObject.entity.ToLower();

                HiveEntityDetail hiveEntityDetail = new HiveEntityDetail();
                var entitylist = (List<dynamic>)hiveEntityDetail.GetEntityList();

                if (!this.IsValidEntity(entitylist, entityname))
                {
                    return new ResponseData() { Success = false, Data = null, Error = "entity is not valid or not available - " + entityname };
                   // return CommonMethods.LogAndReturnBadRequest(this, this.CurrentUserId, "GetData", "entity is not valid or not available - " + entityname);
                }

                if (entityname == "$list")
                {
                    var paramlist = this.ExtractCondition(jsonObject.parameters.ToString());

                    if (paramlist != null && paramlist.Count > 0)
                    {
                        var subentity = string.Empty;

                        foreach (var param in paramlist)
                        {
                            if (param.Item1.ToLower() == "name")
                            {
                                subentity = param.Item3;
                            }
                        }

                        if (!String.IsNullOrEmpty(subentity))
                        {
                            if (!this.IsValidEntity(entitylist, subentity))
                            {
                                return new ResponseData() { Success = false, Data = null, Error = "Parameter name does not contain valid entity name for detail - " + subentity };
                               // return CommonMethods.LogAndReturnBadRequest(this, this.CurrentUserId, "GetData", "Parameter name does not contain valid entity name for detail - " + subentity);
                            }
                            records = (List<dynamic>)hiveEntityDetail.GetEntityColumnDetail(subentity);
                        }
                        else
                            records = entitylist;
                    }
                    else
                    {
                        records = entitylist;
                    }
                }
                else
                {
                    var entityColumns = this.GetColumns((List<dynamic>)hiveEntityDetail.GetEntityColumnDetail(entityname));

                    var conditionColumns = this.ExtractCondition(jsonObject.parameters.ToString());

                    var errMsg = "invalid column";
                    if (!this.IsValidEntityColumns(entityColumns, conditionColumns, out errMsg))
                    {
                        return new ResponseData() { Success = false, Data = null, Error = errMsg };
                       // return CommonMethods.LogAndReturnBadRequest(this, this.CurrentUserId, "GetData", errMsg);
                    }

                    HiveEntityData hiveEntityData = new HiveEntityData(entityname, entityColumns);
                    records = (List<dynamic>)hiveEntityData.GetData(conditionColumns, maxrec);

                }

                if (records != null)
                {
                    result = new JavaScriptSerializer().Serialize(records);
                }

                return new ResponseData() { Success = true, Data = result, Error = null };
            }
            else
            {
                return new ResponseData() { Success = false, Data = null, Error = "entity parameter is either missing or blank" };
               // return CommonMethods.LogAndReturnBadRequest(this, this.CurrentUserId, "GetData", "entity parameter is either missing or blank");
            }
        }

        private bool IsValidEntity(List<dynamic> entitylist,string entityname)
        {
            if (entityname.ToLower() == "$list")
            {
                return true;
            }
            else
            {
                foreach (var entities in entitylist)
                {
                    foreach (var entity in entities)
                    {
                        if (entity.Value.ToLower() == entityname.ToLower())
                        {
                            return true;
                        }
                    }
                }

            }

            return false;
        }

        private bool IsValidEntityColumns(Dictionary<string,string> entityColumns, List<Tuple<string,string, string>> conditionColumns, out string errMsg)
        {
            errMsg = "";

            foreach (var condCol in conditionColumns)
            {
                var isValidCol = false;

                foreach (var entCol in entityColumns)
                {
                    if (entCol.Key.ToLower() == condCol.Item1.ToLower())
                    {
                         isValidCol = true;
 
                    }
                }

                if (!isValidCol)
                {
                    errMsg = "invalid column encountered " + condCol.Item1;
                    return false;
                }
            }

            return true;
        }

        


        private List<Tuple<string,string,string>> ExtractCondition(string jsonParameterString)
        {
            var conditionColumns = new List<Tuple<string, string, string>>();
            var filterCols = new Dictionary<string, object>();

            JsonConvert.PopulateObject(jsonParameterString, filterCols);

            foreach(var cond in filterCols)
            {
                
                var colname = cond.Key;

                if (cond.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject))
                {
                    var extendedCond = new Dictionary<string, string>();
                    JsonConvert.PopulateObject(cond.Value.ToString(), extendedCond);

                    foreach (var subcond in extendedCond)
                    {
                        switch (subcond.Key.ToString().ToLower())
                        {
                            case "gt": 
                                conditionColumns.Add(new Tuple<string, string, string>(colname, ">", subcond.Value.ToString()));
                                break;

                            case "lt":
                                conditionColumns.Add(new Tuple<string, string, string>(colname, "<", subcond.Value.ToString()));
                                break;

                            case "lk":
                                conditionColumns.Add(new Tuple<string, string, string>(colname, "like", subcond.Value.ToString()));
                                break;

                        }
                }
                   
                }
                else if (cond.Value.GetType() == typeof(System.String))
                {

                    conditionColumns.Add(new Tuple<string,string,string>(colname,"=", cond.Value.ToString()));
                }
            }

            return conditionColumns;
        }


        private Dictionary<string, string> GetColumns(List<dynamic> columnDetailObj)
        {
            var columns = new Dictionary<string, string>();

            if (columnDetailObj != null)
            { 
                foreach (var col in columnDetailObj)
                {
                    var colname = string.Empty;
                    var datatype = string.Empty;
                    foreach (var attr in col)
                    {
                        switch (attr.Key) {
                            case "Column": colname = attr.Value; break;
                            case "DataType": datatype = attr.Value; break;
                        }
                    }

                    if (!String.IsNullOrEmpty(colname) && !String.IsNullOrEmpty(datatype))
                    {
                        columns.Add(colname, datatype);
                    }

                }
            }

            return columns;
        }


    }
    

}
