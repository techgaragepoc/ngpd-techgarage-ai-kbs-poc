﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Http.Results;
using Newtonsoft.Json;
using BigDataPOC.HadoopAccess;
using BigDataPOC.Logging;
using BigDataPOC.WebAPI.Entity;
using BigDataPOC.WebAPI.Lib;


namespace BigDataPOC.WebAPI.Controllers
{
    public class LogController : ApiController
    {

       // int maxrec = 1000;
        string CurrentUserId = String.Empty;

        protected internal virtual JsonTextActionResult JsonText(string jsonText)
        {
            return new JsonTextActionResult(Request, jsonText);
        }


        [HttpGet]
        public IHttpActionResult GetLogs()
        {

            try
            {
                if (!CommonMethods.IsRequestValid(this.Request, out this.CurrentUserId))
                    return BadRequest("authentication failed");

                var logs = Logger.GetLog(this.CurrentUserId);

                return JsonText(new JavaScriptSerializer().Serialize(logs));

            }
            catch (Exception ex)
            {
                return CommonMethods.LogAndReturnBadRequest(this, this.CurrentUserId, "GetLogs", ex.Message);
            }
        }
    }
}
