﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BigDataPOC.WebAPI.Models
{
    public class Employee
    {
        public string EMP_NO { get; set; }
        public string BIRTH_DATE { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string GENDER { get; set; }
        public string DOJ { get; set; }
        public string EMPLOYED_FROM { get; set; }
        public string EMPLOYED_TILL { get; set; }
        public string DEPARTMENT { get; set; }
        public string TITLE { get; set; }
        public string TITLE_FROM { get; set; }
        public string TITLE_TILL { get; set; }
        public int SALARY { get; set; }
        public string SALARY_FROM { get; set; }
        public string SALARY_TILL { get; set; }
        public string MGR_EMP_NO { get; set; }
        public string MGR_FIRST_NAME { get; set; }
        public string MGR_LAST_NAME { get; set; }
        public string MGR_FROM { get; set; }
        public string MGR_TILL { get; set; }
    }
}