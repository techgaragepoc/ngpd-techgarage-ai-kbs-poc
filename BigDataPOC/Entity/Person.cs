﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BigDataPOC.WebAPI.Models
{
    public class Person
    {
        public string EMP_NO { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string ADDRESS { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIP { get; set; }
        public string PHONE1 { get; set; }
        public string PHONE2 { get; set; }
        public string EMAIL { get; set; }
        public string WEB { get; set; }
        public string COMPANY_NAME { get; set; }
        public string DOJ { get; set; }
    }
}