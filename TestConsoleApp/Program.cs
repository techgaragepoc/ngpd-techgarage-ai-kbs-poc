﻿using BigDataPOC.HadoopAccess;
using BigDataPOC.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace TestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {

            
            //TestLogger();

            TestEntityList();

//            TestEntityDetail("employee");

            
        }


       
        static void TestEntityList()
        {
            var hiveEntityDetail = new HiveEntityDetail();
            //var maxrec = 300;
            var records = (List<dynamic>)hiveEntityDetail.GetEntityList();

            if (records != null)
            {
                var json = new JavaScriptSerializer().Serialize(records);
                Console.WriteLine(json);
            }

            Console.ReadLine();
        }

        static void TestEntityDetail(string entity)
        {
            var hiveEntityDetail = new HiveEntityDetail();
            //var maxrec = 300;
            var records = (List<dynamic>)hiveEntityDetail.GetEntityColumnDetail(entity);

            if (records != null)
            {
                var json = new JavaScriptSerializer().Serialize(records);
                Console.WriteLine(json);
            }

            Console.ReadLine();
        }


       static void TestLogger()
        {
            Logger.AddLog(new Log() {
                 LogType = LOG_Type.Info,
                 Description = "to test logger",
                 Source = "Test"});

            var logs = Logger.GetLog("AI_Framework");
        }

    }

}
