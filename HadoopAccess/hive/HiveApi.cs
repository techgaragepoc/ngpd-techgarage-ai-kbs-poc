﻿using System;
using System.Collections.Generic;
using System.Linq;
using Syncfusion.ThriftHive.Base;
using System.Dynamic;

namespace BigDataPOC.HadoopAccess
{
    public class HiveApi
    {

        public object ExecuteHiveSql(string sql, string columns, int maxrec)
        {
            //Initialize the connection
            HqlConnection con = new HqlConnection("localhost", 10000, HiveServer.HiveServer2); //, "anonymous", "anonymous");

            //Open the HiveServer connection
            con.Open();

            HqlCommand command = new HqlCommand(sql, con);

            //Execute the query
            HqlDataReader reader = command.ExecuteReader();

            //Assigning number of records to be fetched from Hive database
            if (maxrec > 0)
                reader.FetchSize = maxrec;
            else
                reader.FetchSize = 10000;

            //Fetches the result from the reader and store it in a object
            var result = reader.FetchResult();

            var dataEntity = DataToObject(result, columns);

            //Closing the hive connection 
            con.Close();

            return dataEntity;

        }


        public object GetResult(Guid requestId)
        {

            return new
            {
                status = "", // pending/failed/completed/invalid
                data = ""
            };
        }


        private object DataToObject(HiveResultSet recordset, string columns)
        {
            string[] columnNames = columns.Split(',');
            var result = new List<dynamic>();

            if (recordset != null)
            {

                for (var i = 0; i < recordset.Count; i++)
                {
                    dynamic datarow = new ExpandoObject();
                    HiveRecord item = recordset[i];

                    for (var j = 0; j < item.Count; j++)
                    {
                        AddProperty(datarow, columnNames[j].Trim(), item[j]);
                    }

                    result.Add(datarow);
                }


            }

            return result;
        }


        public static void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            // ExpandoObject supports IDictionary so we can extend it like this
            var expandoDict = expando as IDictionary<string, object>;
            if (expandoDict.ContainsKey(propertyName))
                expandoDict[propertyName] = propertyValue;
            else
                expandoDict.Add(propertyName, propertyValue);
        }

    }
}
