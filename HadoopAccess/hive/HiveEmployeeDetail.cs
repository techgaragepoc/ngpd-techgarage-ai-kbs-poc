﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigDataPOC.HadoopAccess
{
    public class HiveEmployeeDetail
    {
        string columns = "emp_no, birth_date, first_name, last_name, gender, DOJ, employed_from, employed_till, department, title, title_from, title_till, salary, salary_from, salary_till, mgr_emp_no, mgr_first_name, mgr_last_name, mgr_from, mgr_till";
        string ENTITY = "employee";
        HiveApi hiveapi;

        public HiveEmployeeDetail()
        {
            hiveapi = new HiveApi();
        }

        public object GetEmployeeData(int maxrec)
        {
            var sql = "select " + columns + " from " + ENTITY;

            return hiveapi.ExecuteHiveSql(sql, columns, maxrec);
        }

        public object GetEmployeeDataByEmpNo(string empno, int maxrec)
        {
            var sql = "select " + columns + " from " + ENTITY + " where emp_no='" + empno + "'";

            return hiveapi.ExecuteHiveSql(sql, columns, maxrec);
        }

    }
}
