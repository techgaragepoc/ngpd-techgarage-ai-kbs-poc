﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigDataPOC.HadoopAccess
{
    public class HiveEntityData
    {
        //string columns = "emp_no, birth_date, first_name, last_name, gender, DOJ, employed_from, employed_till, department, title, title_from, title_till, salary, salary_from, salary_till, mgr_emp_no, mgr_first_name, mgr_last_name, mgr_from, mgr_till";

        string Entity = string.Empty;
        IDictionary<string, string> Columns = null;
        HiveApi hiveapi;

        public HiveEntityData(string entityname, IDictionary<string, string> columns)
        {
            hiveapi = new HiveApi();
            this.Entity = entityname;
            this.Columns = columns;
        }
                
        public object GetData(List<Tuple<string,string,string>> conditionColumns, int maxrec)
        {
            var colToShow = string.Join(",", this.Columns.Select(x => x.Key).ToList());
            var sql = "select " + colToShow + " from " + this.Entity;

            var condSql = string.Empty;
            if (conditionColumns != null)
            {
                foreach (var cond in conditionColumns)
                {
                    if (!String.IsNullOrEmpty(condSql))
                        condSql += " and ";

                    condSql += (cond.Item1 + " " + cond.Item2 + " '" + cond.Item3 + "'");
                }

            }

            if (!String.IsNullOrEmpty(condSql.Trim())) {
                sql += (" where " + condSql);
            }
            
            return hiveapi.ExecuteHiveSql(sql, colToShow, maxrec);
        }

    }
}
