﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigDataPOC.HadoopAccess
{
    public class HiveEntityDetail
    {
        HiveApi hiveapi;

        public HiveEntityDetail()
        {
            hiveapi = new HiveApi();
        }


        public object GetEntityList()
        {
            var sql = "show tables";
            string columns = "Entity";

            return hiveapi.ExecuteHiveSql(sql, columns, 0);
        }

        public object GetEntityColumnDetail(string entity)
        {
            var sql = "describe " + entity + " ";
            string columns = "Column, DataType, Other";

            return hiveapi.ExecuteHiveSql(sql, columns, 0);
        }
    }
}
