﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigDataPOC.HadoopAccess
{
    public class HivePersonDetail
    {
        string columns = "emp_no, first_name, last_name, address, city, state, zip, phone1, phone2, email, web, company_name, DOJ";
        string ENTITY = "person";
        HiveApi hiveapi;
        
        public HivePersonDetail()
        {
            hiveapi = new HiveApi();
        }
        

        public object GetPersonData(int maxrec)
        {
            var sql = "select " + columns + " from " + ENTITY;

            return hiveapi.ExecuteHiveSql(sql, columns, maxrec);
        }

        public object GetPersonDataByEmpNo(string empno, int maxrec)
        {
            var sql = "select " + columns + " from " + ENTITY + " where emp_no='" + empno + "'";

            return hiveapi.ExecuteHiveSql(sql, columns, maxrec);
        }
    }
}
