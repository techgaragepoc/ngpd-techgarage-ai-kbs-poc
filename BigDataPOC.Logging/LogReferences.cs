﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigDataPOC.Logging
{
    static class LogReferences
    {

        public static string LOG_MongoURL = "mongodb://loguser:password@localhost:27017";
        public static string LOG_Database = "bigdatapoc-logs";
        public static string LOG_Collection_ServiceAccessLogs = "logs";
    }

    public enum LOG_Type
    {
        Info,
        Warning,
        Error
    }
}
