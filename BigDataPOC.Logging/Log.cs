﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigDataPOC.Logging
{
    public class Log
    {
        public string UserId { get; set; }
        public LOG_Type LogType { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string CreatedOn { get; set; }

    }
}
