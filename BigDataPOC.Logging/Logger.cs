﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BigDataPOC.Logging
{
    public static class Logger
    {

        static MongoClient mongoClient = new MongoClient(LogReferences.LOG_MongoURL + @"/" + LogReferences.LOG_Database);
        static IMongoDatabase db = mongoClient.GetDatabase(LogReferences.LOG_Database);

        public static void AddLog(Log log)
        {
            IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>(LogReferences.LOG_Collection_ServiceAccessLogs);
            var document = new BsonDocument
            {
              {"userid", log.UserId.ToString()},
              {"logtype", log.LogType.ToString()},
              {"description", log.Description},
              { "source", log.Source },
              { "createdon", DateTime.UtcNow }
            };

           collection.InsertOne(document);
        }

        public static List<Log> GetLog(string userId)
        {
            var logs = new List<Log>();
            var collection = db.GetCollection<BsonDocument>(LogReferences.LOG_Collection_ServiceAccessLogs);
           
            var cursor = collection.Find(new BsonDocument()).ToList();

            var filteredRec = cursor.Where(c => c["userid"].ToString() == userId && c["createdon"].ToLocalTime().ToShortDateString() == DateTime.Now.ToShortDateString())
                             .OrderByDescending(o => o["createdon"]).Take(100).ToList();

            foreach(var document in filteredRec)
            {
                logs.Add(new Log()
                {
                     UserId = document["userid"].ToString(),
                     LogType = (LOG_Type)Enum.Parse(typeof(LOG_Type),document["logtype"].ToString(),true),
                     Description = document["description"].ToString(),
                     Source = document["source"].ToString(),
                     CreatedOn = document["createdon"].ToLocalTime().ToString()
                });
                
            }

            return logs;
        }


    }
}
